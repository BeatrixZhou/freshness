package com.hkr.freshness;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

/**
 * Main activity, entrance of app
 * User sees the list of watch items
 * @author Xujia Zhou
 */
public class MainActivity extends AppCompatActivity {

    public static final String FILE_NAME = "temp.jpg";
    private static final String ANDROID_CERT_HEADER = "X-Android-Cert";
    private static final String ANDROID_PACKAGE_HEADER = "X-Android-Package";

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int GALLERY_PERMISSIONS_REQUEST = 0;
    private static final int GALLERY_IMAGE_REQUEST = 1;
    public static final int CAMERA_PERMISSIONS_REQUEST = 2;
    public static final int CAMERA_IMAGE_REQUEST = 3;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ListView soonExpireItems;
    public static final String EXTRA_MESSAGE = "com.hkr.freshness.MESSAGE";
    DbAdapter dbAdapter;
    MenuItem itemDelete;
    MenuItem itemSearch;
    MenuItem itemSort;
    HashSet<Long> selectedIds;
    long selectedId; // this is shortclick selected id, for calendar use
    private SearchView searchView;
    SimpleCursorAdapter cursorAdapter;
    int tabPosition = 0;

    private ListView mDrawerList;
    private ArrayAdapter<String> mDrawerAdapter;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String NOTIFICAION_KEY = "notificationOn";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("PREFS_NAME", 0);
        final SharedPreferences.Editor prefEditor = sharedPref.edit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("All"));
        tabLayout.addTab(tabLayout.newTab().setText("Food"));
        tabLayout.addTab(tabLayout.newTab().setText("Cosmetics"));
        tabLayout.addTab(tabLayout.newTab().setText("Other"));

        soonExpireItems = (ListView) findViewById(R.id.soonExpireItems);

        dbAdapter = new DbAdapter(this);

        selectedIds = new HashSet<>();
        this.invalidateOptionsMenu();
        showList(0);

        // Floating button menu
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder
                        .setTitle(R.string.dialog_select_prompt)
                        // .setMessage(R.string.dialog_select_prompt)
                        .setNegativeButton(R.string.dialog_scan_barcode, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                                integrator.setPrompt("Scanning");
                                integrator.setCameraId(0);
                                integrator.setBeepEnabled(false);
                                integrator.setBarcodeImageEnabled(false);
                                integrator.initiateScan();
                            }
                        })
                        .setPositiveButton(R.string.dialog_select_add_item, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent addItemIntent = new Intent(MainActivity.this,
                                        AddItemActivity.class);
                                startActivity(addItemIntent);
                            }
                        });
                builder.create().show();
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedIds.clear();
                tabPosition = tab.getPosition();
                itemDelete.setVisible(false);
                searchView.setQuery("", false);
                showList(tabPosition);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        soonExpireItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor listCursor = (Cursor) parent.getItemAtPosition(position);
                long longSelectedId = listCursor.getLong(listCursor.getColumnIndex(DbHelper.WatchItemsTableKeys.ID_ALIAS));
                if (selectedIds.contains(longSelectedId)) {
                    selectedIds.remove(longSelectedId);
                    ((CardView) view).setCardBackgroundColor(Color.WHITE);
                    if (selectedIds.size()==0) {
                        itemDelete.setVisible(false);
                    }
                } else {
                    selectedIds.add(longSelectedId);
                    ((CardView) view).setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.lightGray300, null));
                    itemDelete.setVisible(true);
                }
                return true;
            }
        });

        soonExpireItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Cursor listCursor = (Cursor) parent.getItemAtPosition(position);
                selectedId = listCursor.getLong(listCursor.getColumnIndex(DbHelper.WatchItemsTableKeys.ID_ALIAS));
                EditWatchItemDialog editWatchItemDialog = new EditWatchItemDialog(MainActivity.this,
                        dbAdapter, selectedId, soonExpireItems, tabPosition);
                DisplayMetrics displayMetrics = new DisplayMetrics();
                MainActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                editWatchItemDialog.getWindow().setLayout(width, height);
                editWatchItemDialog.show();
            }
        });

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("JiajiaGiraffe").withEmail("beatrix.miao@gmail.com").withIcon(getResources().getDrawable(R.drawable.profile))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .build();

        SecondaryDrawerItem item1 = new SecondaryDrawerItem().withIdentifier(1).withName(R.string.notification_on_off);
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(3).withName(R.string.about_us);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (position==1) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            if (sharedPref.getBoolean(NOTIFICAION_KEY, true)) {
                                builder.setMessage("Notification is currently: ON");
                            } else {
                                builder.setMessage("Notification is currently: OFF");
                            }
                            builder
                                    .setTitle(R.string.notification_set)
                                    .setNegativeButton(R.string.notification_on, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            prefEditor.putBoolean(NOTIFICAION_KEY, true);
                                            prefEditor.commit();
                                        }
                                    })
                                    .setPositiveButton(R.string.notification_off, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            prefEditor.putBoolean(NOTIFICAION_KEY, false);
                                            prefEditor.commit();
                                        }
                                    });
                            builder.create().show();
                    }
                        if (position==3) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder
                                    .setTitle(R.string.about_us)
                                    .setMessage(R.string.about_us_msg)
                                    .setNegativeButton(R.string.send_email, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                                            emailIntent.setType("plain/text");
                                            String emailList[] = { "sosostris.star@gmail.com" };
                                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, emailList);
                                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Hello Freshness!");
                                            startActivity(emailIntent);
                                        }
                                    });
                            builder.create().show();
                        }
                        return false;
                    }
                })
                .build();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu_main, menu);
        itemDelete = menu.findItem(R.id.actionDelete);
        itemDelete.setVisible(false);
        itemSearch = menu.findItem(R.id.actionSearch);
        itemSort = menu.findItem(R.id.actionDeleteAllExpired);
        searchView = (SearchView) itemSearch.getActionView();
        itemDelete.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_delete_white_48dp, null));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query) {
                selectedIds.clear();
                itemDelete.setVisible(false);
                Cursor cursor = null;
                if (tabPosition ==0) {
                    cursor = dbAdapter.getAllFilteredWatchItems(query);
                } else {
                    cursor = dbAdapter.getFilteredWatchItemsByCategory(query, tabPosition -1);
                }
                showFilteredList(cursor);
                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query) {
                Cursor cursor = null;
                if (tabPosition ==0) {
                    cursor = dbAdapter.getAllFilteredWatchItems(query);
                } else {
                    cursor = dbAdapter.getFilteredWatchItemsByCategory(query, tabPosition -1);
                }
                showFilteredList(cursor);
                MainActivity.this.invalidateOptionsMenu();
                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    selectedIds.clear();
                    showList(tabPosition);
                    itemDelete.setVisible(false);
                    MainActivity.this.invalidateOptionsMenu();
                    searchView.setIconifiedByDefault(true);
                }
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.actionDelete) {
            dbAdapter.deleteWatchItems(selectedIds);
            showList(tabPosition);
            item.setVisible(false);
            selectedIds.clear();
        }
        if (item.getItemId()==R.id.actionSearch) {
            selectedIds.clear();
            showList(tabPosition);
            itemDelete.setVisible(false);
        }
        if (item.getItemId()==R.id.actionDeleteAllExpired) {
            try {
                deleteAllExpiredItems(tabPosition);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            showList(tabPosition);
        }
        return(super.onOptionsItemSelected(item));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents()==null) {
                Toast.makeText(this, "Scanning cancelled", Toast.LENGTH_LONG).show();
            } else {
                String scannedBarcode = result.getContents();
                Cursor c = dbAdapter.queryItem(scannedBarcode);
                if (c.moveToFirst()) {
                    CustomDialog customDialog = new CustomDialog(this, dbAdapter, scannedBarcode, soonExpireItems, tabPosition);
                    customDialog.show();
                } else {
                    CustomDialogNotFound dialogNotFound = new CustomDialogNotFound(this, scannedBarcode);
                    dialogNotFound.show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showList(int tabPosition) {
        Cursor cursor = null;
        String[] from = { DbHelper.WatchItemsTableKeys.BARCODE, DbHelper.WatchItemsTableKeys.EXPIRE_DATE,
                DbHelper.ItemsTableKeys.NAME, DbHelper.ItemsTableKeys.IMAGE_URL, DbHelper.ItemsTableKeys.CATEGORY};
        int[] to = { R.id.itemBarcode, R.id.itemExpireDate, R.id.itemName, R.id.itemImage, R.id.itemCategory };

        switch (tabPosition) {
            case 0:
                cursor = dbAdapter.queryAllWatchItems();
                break;
            case 1:
                cursor = dbAdapter.queryAllFoodWatchItems();
                break;
            case 2:
                cursor = dbAdapter.queryAllCosmeticsWatchItems();
                break;
            case 3:
                cursor = dbAdapter.queryAllOtherWatchItems();
                break;
        }
        cursorAdapter = new ImageCursorAdapter(this, R.layout.row_main, cursor, from, to);
        soonExpireItems.setAdapter(cursorAdapter);
    }

    private void showFilteredList(Cursor cursor) {
        String[] from = { DbHelper.WatchItemsTableKeys.BARCODE, DbHelper.WatchItemsTableKeys.EXPIRE_DATE,
                DbHelper.ItemsTableKeys.NAME, DbHelper.ItemsTableKeys.IMAGE_URL, DbHelper.ItemsTableKeys.CATEGORY};
        int[] to = { R.id.itemBarcode, R.id.itemExpireDate, R.id.itemName, R.id.itemImage, R.id.itemCategory };
        cursorAdapter = new ImageCursorAdapter(this, R.layout.row_main, cursor, from, to);
        soonExpireItems.setAdapter(cursorAdapter);
    }

    private void deleteAllExpiredItems(int tabPosition) throws ParseException {
        Cursor cursor = null;
        switch (tabPosition) {
            case 0:
                cursor = dbAdapter.queryAllWatchItems();
                break;
            case 1:
                cursor = dbAdapter.queryAllFoodWatchItems();
                break;
            case 2:
                cursor = dbAdapter.queryAllCosmeticsWatchItems();
                break;
            case 3:
                cursor = dbAdapter.queryAllOtherWatchItems();
                break;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        while (cursor.moveToNext()) {
            String expireDate = cursor.getString(cursor.getColumnIndex(DbHelper.WatchItemsTableKeys.EXPIRE_DATE));
            Date expiryDate = dateFormat.parse(expireDate);
            long id = cursor.getLong(cursor.getColumnIndex(DbHelper.WatchItemsTableKeys.ID_ALIAS));
            if (today.after(expiryDate)) {
                dbAdapter.deleteWatchItem(id);
            }
        }
    }

    public void startGalleryChooser() {
        if (PermissionUtils.requestPermission(this, GALLERY_PERMISSIONS_REQUEST, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select a photo"),
                    GALLERY_IMAGE_REQUEST);
        }
    }

    public void startCamera() {
        if (PermissionUtils.requestPermission(
                this,
                CAMERA_PERMISSIONS_REQUEST,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri photoUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", getCameraFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
        }
    }

    public File getCameraFile() {
        File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(dir, FILE_NAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        selectedIds.clear();
        itemDelete.setVisible(false);
        showList(tabPosition);
        searchView.clearFocus();
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSIONS_REQUEST:
                if (PermissionUtils.permissionGranted(requestCode, CAMERA_PERMISSIONS_REQUEST, grantResults)) {
                    startCamera();
                }
                break;
            case GALLERY_PERMISSIONS_REQUEST:
                if (PermissionUtils.permissionGranted(requestCode, GALLERY_PERMISSIONS_REQUEST, grantResults)) {
                    startGalleryChooser();
                }
                break;
        }
    }

    private void addDrawerItems() {
        String[] osArray = { "Enable notification", "Disable notification", "a", "a", "a" };
        mDrawerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mDrawerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showList(tabPosition);
    }

}
