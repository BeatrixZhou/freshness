package com.hkr.freshness.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Local database
 * @author Xujia Zhou
 */
public class DbHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "freshness.db";
    public static final String TABLE_ITEMS = "items"; // unique items
    public static final String TABLE_WATCH_ITEMS = "watch_items";
    public static final int DATABASE_VERSION = 2;

    public static class ItemsTableKeys {
        public static final String BARCODE_ALIAS = "_id";
        public static final String BARCODE = "barcode"; // primary key
        public static final String NAME = "name";
        public static final String IMAGE_URL = "image_url";
        public static final String CATEGORY = "category";
    }

    public static class WatchItemsTableKeys {
        public static final String ID_ALIAS = "_id";
        public static final String ID = "id"; // primary key
        public static final String EXPIRE_DATE = "expire_date";
        public static final String BARCODE = "barcode"; // foreign key
    }

    public static final String CREATE_TABLE_ITEMS = "create table " + TABLE_ITEMS + " ( "
            + ItemsTableKeys.BARCODE + " text primary key, " + ItemsTableKeys.NAME + " text not null, "
            + ItemsTableKeys.IMAGE_URL + " text, " + ItemsTableKeys.CATEGORY + " integer );";

    public static final String CREATE_TABLE_WATCHLIST = "create table " + TABLE_WATCH_ITEMS + " ( "
            + WatchItemsTableKeys.ID + " integer primary key autoincrement, " + WatchItemsTableKeys.EXPIRE_DATE +
            " text not null, " + WatchItemsTableKeys.BARCODE + " text not null, " + "FOREIGN KEY ("
            + ItemsTableKeys.BARCODE + ") REFERENCES " + TABLE_ITEMS + " (" + ItemsTableKeys.BARCODE + ")" + ");";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ITEMS);
        db.execSQL(CREATE_TABLE_WATCHLIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table " + TABLE_ITEMS);
        db.execSQL("drop table " + TABLE_WATCH_ITEMS);
        onCreate(db);
    }

}