package com.hkr.freshness.db;

import com.google.common.base.Objects;

/**
 * Class representing a unique item
 * @author Xujia Zhou
 */
public class Item {

    private String barcode;
    private String name;
    private String imageURL;
    private Category category;
    private boolean selected;

    public Item(String barcode) {
        this.barcode = barcode;
        selected = false;
    }

    public Item(String barcode, String name, String imageURL, Category category) {
        this.barcode = barcode;
        this.name = name;
        this.imageURL = imageURL;
        this.category = category;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final Item other = (Item) obj;
        return Objects.equal(this.barcode, other.barcode)
                && Objects.equal(this.name, other.name)
                && Objects.equal(this.imageURL, other.imageURL);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                this.barcode, this.name, this.imageURL);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("barcode", barcode)
                .add("name", name)
                .add("imageURL", imageURL)
                .toString();
    }
}
