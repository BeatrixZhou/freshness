package com.hkr.freshness.db;

import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;

import static com.hkr.freshness.db.DbHelper.*;

/**
 * Local database methods
 * @author Xujia Zhou
 */
public class DbAdapter {
    SQLiteDatabase db;
    DbHelper dbHelper;
    Context context;

    public DbAdapter(Context c) {
        context = c;
    }

    public DbAdapter openToRead() {
        dbHelper = new DbHelper(context);
        db = dbHelper.getReadableDatabase();
        return this;
    }

    public DbAdapter openToWrite() {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        db.close();
    }

    // FOOD 0, COSMETICS 1, OTHER 2
    public void createItem(String barcode, String name, String imageURL, int category) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ItemsTableKeys.BARCODE, barcode);
        contentValues.put(ItemsTableKeys.NAME, name);
        contentValues.put(ItemsTableKeys.IMAGE_URL, imageURL);
        contentValues.put(ItemsTableKeys.CATEGORY, category);

        openToWrite();

        db.insert(TABLE_ITEMS, null, contentValues);
        close();
    }

    public void updateItem(String barcode, String newBarcode, String name, String imageURL, int category, boolean imageChanged) {
        if (imageChanged) {
            deleteItem(barcode);
        } else {
            deleteItemWithoutImage(barcode);
        }
        createItem(newBarcode, name, imageURL, category);
    }

    public void deleteItem(String barcode) {
        openToWrite();
        deleteImage(barcode);
        db.delete(TABLE_ITEMS,
                ItemsTableKeys.BARCODE + " = " + "'" + barcode + "'", null);
        close();
    }

    public void deleteItemWithoutImage(String barcode) {
        openToWrite();
        db.delete(TABLE_ITEMS,
                ItemsTableKeys.BARCODE + " = " + "'" + barcode + "'", null);
        close();
    }

    public void deleteItemsByBarcodes(HashSet<String> barcodes) {
        for (String barcode : barcodes) {
            deleteItem(barcode);
        }
    }

    public void deleteAllItems() {
        openToWrite();
        Cursor c = queryAllItems();
        while (c.moveToNext()) {
            String barcode = c.getString(c.getColumnIndex(ItemsTableKeys.BARCODE_ALIAS));
            deleteItem(barcode);
        }
        /*db.execSQL("delete from "+ TABLE_ITEMS);
        close();*/
    }

    public Cursor queryItem(String barcode) {
        String rawQuery = String.format("SELECT %s AS _id, %s, %s, %s FROM %s WHERE %s = '%s'",
                ItemsTableKeys.BARCODE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_ITEMS, ItemsTableKeys.BARCODE_ALIAS, barcode);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    public Cursor queryAllItems() {
        String rawQuery = String.format("SELECT %s AS _id, %s, %s, %s FROM %s ORDER BY %s",
                ItemsTableKeys.BARCODE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_ITEMS, ItemsTableKeys.NAME);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    public Cursor queryAllFoodItems() {
        return queryItemsByCategory(Category.FOOD);
    }

    public Cursor queryAllCosmeticsItems() {
        return queryItemsByCategory(Category.COSMETICS);
    }

    public Cursor queryAllOtherItems() {
        return queryItemsByCategory(Category.OTHER);
    }

    // In class Watchlist expireDate is of type Date, not String
    public long createWatchItem(String barcode, String expireDate) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(WatchItemsTableKeys.BARCODE, barcode);
        contentValues.put(WatchItemsTableKeys.EXPIRE_DATE, expireDate);
        openToWrite();
        long id = db.insert(TABLE_WATCH_ITEMS, null, contentValues);
        close();
        return id;
    }

    public void updateWatchItem(long id, String barcode, String expireDate) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(WatchItemsTableKeys.EXPIRE_DATE, expireDate);
        openToWrite();
        db.update(TABLE_WATCH_ITEMS, contentValues,
                WatchItemsTableKeys.ID + " = " + id, null);
        close();
    }

    public Cursor queryWatchItem(long id) {
        String rawQuery = String.format("SELECT %s, %s.%s, %s, %s, %s, %s FROM %s INNER JOIN %s ON %s.%s = %s.%s AND %s.%s = %d",
                WatchItemsTableKeys.ID, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE, WatchItemsTableKeys.EXPIRE_DATE,
                ItemsTableKeys.NAME, ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_WATCH_ITEMS, TABLE_ITEMS,
                TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE, TABLE_ITEMS, ItemsTableKeys.BARCODE,
                TABLE_WATCH_ITEMS, WatchItemsTableKeys.ID, id);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    public void deleteWatchItem(String barcode) {
        openToWrite();
        db.delete(TABLE_WATCH_ITEMS,
                WatchItemsTableKeys.BARCODE + " = " + "'" + barcode + "'", null);
        close();
    }

    public void deleteWatchItem(long id) {
        openToWrite();
        db.delete(TABLE_WATCH_ITEMS,
                WatchItemsTableKeys.ID + " = " + id, null);
        close();
    }

    public void deleteWatchItems(HashSet<Long> ids) {
        openToWrite();
        for (long id: ids) {
            deleteWatchItem(id);
        }
        close();
    }

    public void deleteAllWatchItems() {
        openToWrite();
        db.execSQL("delete from "+ TABLE_WATCH_ITEMS);
        close();
    }

    public Cursor queryAllWatchItems() {

        // SELECT watch_items.id AS _id, barcode, expire_date, name, image_url, category FROM
        // watch_items INNER JOIN items
        String rawQuery = String.format("SELECT %s AS _id, %s.%s, %s, %s, %s, %s FROM %s INNER JOIN %s ON %s.%s = %s.%s ORDER BY %s",
                WatchItemsTableKeys.ID, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE,
                WatchItemsTableKeys.EXPIRE_DATE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_WATCH_ITEMS,
                TABLE_ITEMS, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE,
                TABLE_ITEMS, ItemsTableKeys.BARCODE, WatchItemsTableKeys.EXPIRE_DATE);
        openToWrite();

        // can't close() because the connection needs to be kept there when cursor is used
        Cursor c = db.rawQuery(rawQuery, null);
        return c;

    }

    public Cursor queryAllFoodWatchItems() {
        return queryWatchItemsByCategory(Category.FOOD);
    }

    public Cursor queryAllCosmeticsWatchItems() {
        return queryWatchItemsByCategory(Category.COSMETICS);
    }

    public Cursor queryAllOtherWatchItems() {
        return queryWatchItemsByCategory(Category.OTHER);
    }

    public Cursor queryWatchItemsByCategory(Category category) {
        String rawQuery = String.format("SELECT %s AS _id, %s.%s, %s, %s, %s, %s FROM %s INNER JOIN %s ON %s.%s = %s.%s AND %s = %d ORDER BY %s",
                WatchItemsTableKeys.ID, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE, WatchItemsTableKeys.EXPIRE_DATE,
                ItemsTableKeys.NAME, ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_WATCH_ITEMS, TABLE_ITEMS,
                TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE, TABLE_ITEMS, ItemsTableKeys.BARCODE,
                ItemsTableKeys.CATEGORY, category.ordinal(), WatchItemsTableKeys.EXPIRE_DATE);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    public Cursor queryItemsByCategory(Category category) {
        String rawQuery = String.format("SELECT %s AS _id, %s, %s, %s FROM %s WHERE %s = %d ORDER BY %s",
                ItemsTableKeys.BARCODE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_ITEMS,
                ItemsTableKeys.CATEGORY, category.ordinal(), ItemsTableKeys.NAME);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    // SELECT FROM table_name WHERE column LIKE '%XXXX%'
    public Cursor getFilteredWatchItemsByCategory(String filterText, int category) {
        String rawQuery = String.format("SELECT %s AS _id, %s.%s, %s, %s, %s, %s FROM %s INNER JOIN %s ON" +
                        " %s.%s = %s.%s AND %s = %d AND %s LIKE '%s%s%s' ORDER BY %s",
                WatchItemsTableKeys.ID, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE,
                WatchItemsTableKeys.EXPIRE_DATE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_WATCH_ITEMS,
                TABLE_ITEMS, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE,
                TABLE_ITEMS, ItemsTableKeys.BARCODE, ItemsTableKeys.CATEGORY, category,
                ItemsTableKeys.NAME, "%", filterText, "%", WatchItemsTableKeys.EXPIRE_DATE);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    public Cursor getAllFilteredWatchItems(String filterText) {
        String rawQuery = String.format("SELECT %s AS _id, %s.%s, %s, %s, %s, %s FROM %s INNER JOIN %s ON" +
                        " %s.%s = %s.%s AND %s LIKE '%s%s%s' ORDER BY %s",
                WatchItemsTableKeys.ID, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE,
                WatchItemsTableKeys.EXPIRE_DATE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_WATCH_ITEMS,
                TABLE_ITEMS, TABLE_WATCH_ITEMS, WatchItemsTableKeys.BARCODE,
                TABLE_ITEMS, ItemsTableKeys.BARCODE, ItemsTableKeys.NAME, "%", filterText, "%", WatchItemsTableKeys.EXPIRE_DATE);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    public Cursor getFilteredItemsByCategory(String filterText, int category) {
        String rawQuery = String.format("SELECT %s AS _id, %s, %s, %s FROM %s WHERE %s = %d AND %s LIKE '%s%s%s' ORDER BY %s",
                ItemsTableKeys.BARCODE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_ITEMS, ItemsTableKeys.CATEGORY,
                category, ItemsTableKeys.NAME, "%", filterText, "%", ItemsTableKeys.NAME);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    public Cursor getAllFilteredItems(String filterText) {
        String rawQuery = String.format("SELECT %s AS _id, %s, %s, %s FROM %s WHERE %s LIKE '%s%s%s' ORDER BY %s",
                ItemsTableKeys.BARCODE, ItemsTableKeys.NAME,
                ItemsTableKeys.IMAGE_URL, ItemsTableKeys.CATEGORY, TABLE_ITEMS,
                ItemsTableKeys.NAME, "%", filterText, "%", ItemsTableKeys.NAME);
        openToWrite();
        Cursor c = db.rawQuery(rawQuery, null);
        return c;
    }

    private void deleteImages(HashSet<String> barcodes) {
        for (String barcode : barcodes) {
            Cursor c = queryItem(barcode);
            c.moveToFirst();
            String imageURL = c.getString(c.getColumnIndex(ItemsTableKeys.IMAGE_URL));
            File imageToDelete = new File(context.getFilesDir() + "/saved_images/" + imageURL);
            if (imageToDelete.exists()) {
                if (imageToDelete.delete()) {
                    System.out.println("file Deleted :" + imageURL);
                } else {
                    System.out.println("file not Deleted :" + imageURL);
                }
            }
        }
    }

    private void deleteImage(String barcode) {
        Cursor c = queryItem(barcode);
        c.moveToFirst();
        String imageURL = c.getString(c.getColumnIndex(ItemsTableKeys.IMAGE_URL));
        File imageToDelete = new File(context.getFilesDir() + "/saved_images/" + imageURL);
        if (imageToDelete.exists()) {
            if (imageToDelete.delete()) {
                System.out.println("file Deleted :" + imageURL);
            } else {
                System.out.println("file not Deleted :" + imageURL);
            }
        }
    }

}