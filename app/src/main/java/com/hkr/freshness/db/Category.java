package com.hkr.freshness.db;

/**
 * Enum class representing category of items
 * @author Xujia Zhou
 */
public enum Category {
    FOOD, COSMETICS, OTHER
}