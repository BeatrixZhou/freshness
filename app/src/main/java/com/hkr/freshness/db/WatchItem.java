package com.hkr.freshness.db;

import java.util.Date;

/**
 * Class representing a watch item, that is, an item that is going to  within a definite amount of time
 * @author Xujia Zhou
 */
public class WatchItem {

    private long id;
    private Date expireDate;
    private String barcode;
    private Category category;
    private boolean selected;

    public WatchItem(long id, Date expireDate, String barcode, Category category) {
        this.id = id;
        this.expireDate = expireDate;
        this.barcode = barcode;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
