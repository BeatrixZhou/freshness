package com.hkr.freshness;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Activity where user edits a selected unique itemDelete
 * @author Xujia Zhou
 */
public class EditItem extends AppCompatActivity {

    private static final int GALLERY_PERMISSIONS_REQUEST = 0;
    private static final int GALLERY_IMAGE_REQUEST = 1;
    public static final int CAMERA_PERMISSIONS_REQUEST = 2;
    public static final int CAMERA_IMAGE_REQUEST = 3;

    private Button clearButton, okButton;
    ImageButton buttonCamera, buttonGallery, buttonScanner;
    private Spinner editCategory;
    EditText editItemName, editItemBarcode;
    private ImageView imageView;
    String nameValue, imageURLValue, barcodeValue;
    int categoryValue;
    private DbAdapter dbAdapter;
    private String selectedBarcode = "";
    private Bitmap bitmap;
    boolean imageChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbAdapter = new DbAdapter(this);
        editCategory = (Spinner) findViewById(R.id.editItemCategory);
        editCategory.setAdapter(new ArrayAdapter<Category>(this, R.layout.spinner_row, Category.values()));
        clearButton = (Button) findViewById(R.id.clearButton);
        okButton = (Button) findViewById(R.id.okButton);
        buttonCamera = (ImageButton) findViewById(R.id.buttonCamera);
        buttonGallery = (ImageButton) findViewById(R.id.buttonGallery);
        buttonScanner = (ImageButton) findViewById(R.id.buttonScanner);

        editItemName = (EditText) findViewById(R.id.editItemName);
        imageView = (ImageView) findViewById(R.id.imageView);
        editItemBarcode = (EditText) findViewById(R.id.editItemBarcode);

        imageChanged = false;

        Intent intent = getIntent();
        selectedBarcode = intent.getStringExtra(AddItemActivity.EXTRA_MESSAGE);
        Cursor c = dbAdapter.queryItem(selectedBarcode);
        c.moveToFirst();
        editItemName.setText(c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.NAME)));
        editItemBarcode.setText(c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.BARCODE_ALIAS)));
        final String imageURL = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.IMAGE_URL));
        if (imageURL != null) {
            if (imageURL.equals("very_very_cute_mouse")) {
                Uri uri = Uri.parse("android.resource://com.hkr.freshness/drawable/very_very_cute_mouse");
                imageView.setImageURI(uri);
            } else {
                Uri uri = Uri.parse(this.getFilesDir() + "/saved_images/" + imageURL);
                imageView.setImageURI(uri);
            }
        }
        editCategory.setSelection(c.getInt(c.getColumnIndex(DbHelper.ItemsTableKeys.CATEGORY)));

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nameValue = editItemName.getText().toString();
                barcodeValue = editItemBarcode.getText().toString();

                if (nameValue==null || nameValue.equals("")) {
                    Toast.makeText(getBaseContext(), "Item name can't be empty", Toast.LENGTH_LONG).show();
                    return;
                }

                String pattern ="^[0-9]+$";
                Matcher matcher = Pattern.compile(pattern).matcher(barcodeValue);
                if (barcodeValue==null || !matcher.find()) {
                    Toast.makeText(getBaseContext(), "Invalid barcode", Toast.LENGTH_LONG).show();
                    return;
                }

                String[] strArray = nameValue.split(" ");
                StringBuilder strBuilder = new StringBuilder();
                for (int i=0; i<strArray.length; i++) {
                    String cappedStr = strArray[i].substring(0, 1).toUpperCase() + strArray[i].substring(1);
                    if (i==strArray.length-1) {
                        strBuilder.append(cappedStr);
                    } else {
                        strBuilder.append(cappedStr + " ");
                    }
                }
                String nameValueUpperCased = strBuilder.toString();

                if (imageChanged) {
                    imageURLValue = saveImage(bitmap);
                } else {
                    imageURLValue = imageURL;
                }

                categoryValue = editCategory.getSelectedItemPosition();

                dbAdapter.updateItem(selectedBarcode, barcodeValue, nameValueUpperCased, imageURLValue, categoryValue, imageChanged);

                finish();

                Intent intent = new Intent(EditItem.this, AddItemActivity.class);
                startActivity(intent);
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editItemName.setText("");
                editItemBarcode.setText("");
            }
        });

        buttonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCamera();
            }
        });

        buttonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGalleryChooser();
            }
        });

        buttonScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(EditItem.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scanning");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
            }
        });

    }

    public void startGalleryChooser() {
        if (PermissionUtils.requestPermission(
                this,
                GALLERY_PERMISSIONS_REQUEST,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select a photo"),
                    GALLERY_IMAGE_REQUEST);
        }
    }

    public void startCamera() {
        if (PermissionUtils.requestPermission(
                this,
                CAMERA_PERMISSIONS_REQUEST,
                Manifest.permission.CAMERA)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // intent.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
            /*Uri photoUri = FileProvider.getUriForFile(this, "com.hkr.freshness.fileProvider", getCameraFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);*/
            startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
        } else {
            Toast.makeText(getBaseContext(), "Camera is not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            /*File out = new File(getFilesDir(), "newImage.jpg");
            if(!out.exists()) {
                Toast.makeText(getBaseContext(),
                        "Error while capturing image", Toast.LENGTH_LONG)
                        .show();
                return;
            }
            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());*/
            imageChanged = true;
            bitmap = (Bitmap) data.getExtras().get("data");
            /*Uri imageUri = data.getData();
            imageUri.toString();
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");*/
            imageView.setImageBitmap(bitmap);
        }

        if (requestCode == GALLERY_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                imageChanged = true;
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents()==null) {
                    Toast.makeText(this, "Scanning cancelled", Toast.LENGTH_LONG).show();
                } else {
                    String scannedBarcode = result.getContents();
                    editItemBarcode.setText(scannedBarcode);
                }
            }
        }

    }

    private String saveImage(Bitmap bitmap) {
        File dir = getFilesDir();
        String fileName = Long.toString(System.currentTimeMillis());
        File myDir = new File(dir + "/saved_images");
        if (!myDir.exists()) {
            myDir.mkdir();
        }
        File file = new File (myDir, fileName);
        try (FileOutputStream out = new FileOutputStream(file)) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

}
