package com.hkr.freshness;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;

import android.app.DatePickerDialog;
import android.widget.Toast;

/**
 * Activity where user may choose from the list of unique items to add to watch list
 * User can also add new unique itemDelete by click floating button
 * User can also delete items
 * @author Xujia Zhou
 */
public class AddItemActivity extends AppCompatActivity {

    private ListView itemList;
    public static final String EXTRA_MESSAGE = "com.hkr.freshness.MESSAGE";
    DbAdapter dbAdapter;
    private MenuItem itemDelete;
    private MenuItem itemEdit;
    HashSet<String> selectedBarcodes;
    String lastSelectedBarcode;
    private TabLayout tabLayout;
    MenuItem itemSearch;
    private SearchView searchView;
    SimpleCursorAdapter cursorAdapter;
    int tabPosition;
    String expireDate = "";
    long newItemId = 0;

    private int currentYear, currentMonth, currentDay;
    private final static long MONTH_IN_MILLISECONDS = 30 * 24 * 3600 * 1000;
    private final static long WEEK_IN_MILLISECONDS = 7 * 24 * 3600 * 1000;
    private final static long DAY_IN_MILLISECONDS = 24 * 3600 * 1000;
    private final static long HOUR_IN_MILLISECONDS = 3600 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedBarcodes = new HashSet<>();
        lastSelectedBarcode = "";
        tabPosition = 0;
        dbAdapter = new DbAdapter(this);
        this.invalidateOptionsMenu();

        getSupportActionBar().setElevation(0);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("All"));
        tabLayout.addTab(tabLayout.newTab().setText("Food"));
        tabLayout.addTab(tabLayout.newTab().setText("Cosmetics"));
        tabLayout.addTab(tabLayout.newTab().setText("Other"));

        itemList = (ListView) findViewById(R.id.itemList);
        registerForContextMenu(itemList);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        showList(0);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddItemActivity.this,
                        CreateItemActivity.class);
                startActivity(intent);
            }
        });

        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor listCursor = (Cursor) parent.getItemAtPosition(position);
                final String newItemBarcode = listCursor.getString(listCursor.getColumnIndex(DbHelper.ItemsTableKeys.BARCODE_ALIAS));
                final String itemName = listCursor.getString(listCursor.getColumnIndex(DbHelper.ItemsTableKeys.NAME));
                final Calendar calendar = Calendar.getInstance();
                currentYear = calendar.get(Calendar.YEAR);
                currentMonth = calendar.get(Calendar.MONTH);
                currentDay = calendar.get(Calendar.DAY_OF_MONTH);

                // Keep in mind that months values start from 0, so October is actually month number 9
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddItemActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                String dateString, monthString;
                                if (dayOfMonth < 10) {
                                    dateString = "0" + dayOfMonth;
                                } else {
                                    dateString = Integer.toString(dayOfMonth);
                                }
                                if (month < 9) {
                                    monthString = "0" + Integer.toString(month+1);
                                } else {
                                    monthString = Integer.toString(month+1);
                                }
                                expireDate = String.format("%d-%s-%s", year, monthString, dateString);
                                newItemId = dbAdapter.createWatchItem(newItemBarcode, expireDate);
                                Toast.makeText(getBaseContext(), itemName + " added to watch list", Toast.LENGTH_LONG).show();
                                scheduleNotifications(year, month, dayOfMonth, itemName);
                            }}, currentYear, currentMonth, currentDay);
                datePickerDialog.getDatePicker().setMinDate(new Date().getTime() + DAY_IN_MILLISECONDS);
                datePickerDialog.show();
            }
                /*Intent intent = new Intent(AddItemActivity.this,
                        CreateItemActivity.class);
                intent.putExtra(EXTRA_MESSAGE, newItemBarcode);
                startActivity(intent);*/

        });

        itemList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor listCursor = (Cursor) parent.getItemAtPosition(position);
                String selectedBarcode = listCursor.getString(listCursor.getColumnIndex(DbHelper.ItemsTableKeys.BARCODE_ALIAS));
                if (selectedBarcodes.contains(selectedBarcode)) {
                    selectedBarcodes.remove(selectedBarcode);
                    ((CardView) view).setCardBackgroundColor(Color.WHITE);
                    if (selectedBarcodes.size()==1) {
                        itemEdit.setVisible(true);
                    }
                    if (selectedBarcodes.size()==0) {
                        itemDelete.setVisible(false);
                        itemEdit.setVisible(false);
                    }
                } else {
                    selectedBarcodes.add(selectedBarcode);
                    lastSelectedBarcode = selectedBarcode;
                    ((CardView) view).setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.lightGray300, null));
                    if (selectedBarcodes.size()==1) {
                        itemEdit.setVisible(true);
                        itemDelete.setVisible(true);
                    } else {
                        itemEdit.setVisible(false);
                    }
                }
                return true;
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedBarcodes.clear();
                tabPosition = tab.getPosition();
                itemDelete.setVisible(false);
                itemEdit.setVisible(false);
                searchView.setQuery("", false);
                // searchView.clearFocus();
                showList(tabPosition);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        itemDelete = menu.findItem(R.id.actionDelete);
        itemDelete.setVisible(false);
        itemEdit = menu.findItem(R.id.actionEdit);
        itemEdit.setVisible(false);
        itemEdit.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_mode_edit_white_48dp, null));
        itemDelete.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_delete_white_48dp, null));
        itemSearch = menu.findItem(R.id.actionSearch);
        searchView = (SearchView) itemSearch.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query) {
                selectedBarcodes.clear();
                itemDelete.setVisible(false);
                itemEdit.setVisible(false);
                Cursor cursor = null;
                int position = tabLayout.getSelectedTabPosition();
                if (position==0) {
                    cursor = dbAdapter.getAllFilteredItems(query);
                } else {
                    cursor = dbAdapter.getFilteredItemsByCategory(query, position-1);
                }
                showFilteredList(cursor);
                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query) {
                Cursor cursor = null;
                if (tabPosition ==0) {
                    cursor = dbAdapter.getAllFilteredItems(query);
                } else {
                    cursor = dbAdapter.getFilteredItemsByCategory(query, tabPosition - 1);
                }
                showFilteredList(cursor);
                AddItemActivity.this.invalidateOptionsMenu();
                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    selectedBarcodes.clear();
                    showList(tabPosition);
                    itemDelete.setVisible(false);
                    itemEdit.setVisible(false);
                    AddItemActivity.this.invalidateOptionsMenu();
                    searchView.setIconifiedByDefault(true);
                }
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.actionDelete) {
            dbAdapter.deleteItemsByBarcodes(selectedBarcodes);
            // int position = tabLayout.getSelectedTabPosition();
            showList(tabPosition);
            itemEdit.setVisible(false);
            itemDelete.setVisible(false);
            selectedBarcodes.clear();
            lastSelectedBarcode = "";
        } else if (item.getItemId()==R.id.actionEdit) {
            Intent intent = new Intent(AddItemActivity.this,
                    EditItem.class);
            intent.putExtra(EXTRA_MESSAGE, lastSelectedBarcode);
            itemEdit.setVisible(false);
            itemDelete.setVisible(false);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.actionSearch) {
            selectedBarcodes.clear();
            showList(tabPosition);
            itemDelete.setVisible(false);
            itemEdit.setVisible(false);
        }
        return(super.onOptionsItemSelected(item));
    }

    private void showList(int category) {
        Cursor cursor = null;
        String[] from = {DbHelper.ItemsTableKeys.BARCODE_ALIAS, DbHelper.ItemsTableKeys.NAME, DbHelper.ItemsTableKeys.IMAGE_URL};
        int[] to = {R.id.itemBarcode, R.id.itemName, R.id.itemImage};

        switch (category) {
            case 0:
                cursor = dbAdapter.queryAllItems();
                break;
            case 1:
                cursor = dbAdapter.queryAllFoodItems();
                break;
            case 2:
                cursor = dbAdapter.queryAllCosmeticsItems();
                break;
            case 3:
                cursor = dbAdapter.queryAllOtherItems();
                break;
        }

        cursorAdapter = new ImageCursorAdapterWithoutExpire(this, R.layout.row_no_expire_date, cursor, from, to);
        itemList.setAdapter(cursorAdapter);
    }

    private void showFilteredList(Cursor cursor) {
        String[] from = {DbHelper.ItemsTableKeys.BARCODE_ALIAS, DbHelper.ItemsTableKeys.NAME, DbHelper.ItemsTableKeys.IMAGE_URL};
        int[] to = {R.id.itemBarcode, R.id.itemName, R.id.itemImage};
        cursorAdapter = new ImageCursorAdapterWithoutExpire(this, R.layout.row_no_expire_date, cursor, from, to);
        itemList.setAdapter(cursorAdapter);
    }

    private void scheduleNotification(Notification notification, long delayInMilliSeconds) {

        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        int notificationId = UUID.randomUUID().hashCode();
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        notificationIntent.putExtra(NotificationPublisher.ITEM_ID, Long.toString(newItemId));
        notificationIntent.putExtra(NotificationPublisher.ITEM_EXPIRE_DATE, expireDate);

        // ONE_SHOT creates a new intent instead of replacing the old one
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, notificationId, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        long futureInMillis = SystemClock.elapsedRealtime() + delayInMilliSeconds;
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    private Notification buildNotification(String title, String content) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setSmallIcon(R.mipmap.ic_launcher_freshness);
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        // clear notification from notification bar after it is clicked
        builder.setAutoCancel(true);
        return builder.build();
    }

    private void scheduleNotifications(int year, int month, int dayOfMonth, String itemName) {
        Date futureDate = new Date(year-1900, month, dayOfMonth);
        Date today = new Date();

        Notification beforeExpiry = buildNotification("An item will expire soon!", itemName + " expires on " + expireDate);
        Notification afterExpiry = buildNotification("An item has expired!", itemName + " expired on " + expireDate);

        long timeDifference = futureDate.getTime() - today.getTime();

        if (timeDifference > MONTH_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - MONTH_IN_MILLISECONDS);
        }
        if (timeDifference > WEEK_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - WEEK_IN_MILLISECONDS);
        }
        if (timeDifference > DAY_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - DAY_IN_MILLISECONDS);
        } else {
            scheduleNotification(beforeExpiry, 5000);
        }
        scheduleNotification(afterExpiry, timeDifference + HOUR_IN_MILLISECONDS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        selectedBarcodes.clear();
        itemDelete.setVisible(false);
        itemEdit.setVisible(false);
        lastSelectedBarcode = "";
        showList(tabPosition);
        searchView.clearFocus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        selectedBarcodes.clear();
        lastSelectedBarcode = "";
        showList(0);
    }

}
