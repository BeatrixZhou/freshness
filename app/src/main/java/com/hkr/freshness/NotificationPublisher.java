package com.hkr.freshness;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;

import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;

/**
 * This class will publish a notification when the alarm is triggered
 * @author Xujia Zhou
 */
public class NotificationPublisher extends BroadcastReceiver {

    public static final String NOTIFICATION_ID = "notification_id";
    public static final String NOTIFICATION = "notification";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_EXPIRE_DATE = "item_expire_date";

    public static final String PREFS_NAME = "MyPrefsFile";

    public void onReceive(Context context, Intent intent) {

        SharedPreferences sharedPref = context.getSharedPreferences("PREFS_NAME", 0);

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        // if there is no notification_id then use default value 0
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
        String itemIdInString = intent.getStringExtra(ITEM_ID);
        long itemId = Long.parseLong(itemIdInString);
        String expectedExpireDate = intent.getStringExtra(ITEM_EXPIRE_DATE);

        DbAdapter dbAdapter = new DbAdapter(context);
        Cursor c = dbAdapter.queryWatchItem(itemId);
        if (c.moveToFirst()) {
            String ActualExpireDate = c.getString(c.getColumnIndex(DbHelper.WatchItemsTableKeys.EXPIRE_DATE));
            if (ActualExpireDate.equals(expectedExpireDate)) {
                if (sharedPref.getBoolean(MainActivity.NOTIFICAION_KEY, true)) {
                    notificationManager.notify(id, notification);
                }
            }
        }
    }

}
