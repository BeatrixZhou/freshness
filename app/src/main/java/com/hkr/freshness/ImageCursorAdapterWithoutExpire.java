package com.hkr.freshness;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbHelper;

/**
 * A custom CursorAdapter class binding row with row view without expire date
 * @author Xujia Zhou
 */
public class ImageCursorAdapterWithoutExpire extends SimpleCursorAdapter {

    private Cursor c;
    private Context context;
    boolean[] itemChecked;

    public ImageCursorAdapterWithoutExpire(Context context, int layout, Cursor cursor, String[] from, int[] to) {
        super(context, layout, cursor, from, to, 0);
        this.context = context;
    }

    @Override
    public void bindView(View inView, Context context, Cursor cursor) {
        View view = inView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_no_expire_date, null);
        }
        String barcode = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.BARCODE_ALIAS));
        String name = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.NAME));
        String imageURL = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.IMAGE_URL));
        ImageView imageView = (ImageView) view.findViewById(R.id.itemImage);
        int category = cursor.getInt(cursor.getColumnIndex(DbHelper.ItemsTableKeys.CATEGORY));
        String categoryValue = Category.values()[category].name();
        if (imageURL != null) {
            if (imageURL.equals("very_very_cute_mouse")) {
                Uri uri = Uri.parse("android.resource://com.hkr.freshness/drawable/very_very_cute_mouse");
                imageView.setImageURI(uri);
            } else {
                Uri uri = Uri.parse(context.getFilesDir() + "/saved_images/" + imageURL);
                imageView.setImageURI(uri);
            }
        }
        TextView itemBarcode = (TextView) view.findViewById(R.id.itemBarcode);
        itemBarcode.setText(barcode);

        TextView itemName = (TextView) view.findViewById(R.id.itemName);
        itemName.setText(name);

        TextView itemCategory = (TextView) view.findViewById(R.id.itemCategory);
        itemCategory.setText(categoryValue);
        if (categoryValue.equals("FOOD")) {
            itemCategory.setTextColor(ContextCompat.getColor(context, R.color.foodColor));
        } else if (categoryValue.equals("COSMETICS")) {
            itemCategory.setTextColor(ContextCompat.getColor(context, R.color.cosmeticsColor));
        } else {
            itemCategory.setTextColor(ContextCompat.getColor(context, R.color.lightGray400));
        }
    }

}