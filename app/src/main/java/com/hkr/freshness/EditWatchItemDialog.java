package com.hkr.freshness;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.res.ResourcesCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.UUID;

/**
 * Created by XUZH0001 on 5/17/2017.
 */

public class EditWatchItemDialog extends Dialog implements android.view.View.OnClickListener {

    private Button saveButton, cancelButton;
    private ImageButton buttonCalendar;
    private TextView itemName, itemCategory, itemBarcode, itemExpireDate;
    private ImageView itemImage;
    private DbAdapter dbAdapter;
    private Context context;
    private String newExpireDate;
    private long itemId;
    private ListView list;
    private int tabPosition;

    int currentYear, currentMonth, currentDay;

    private final static long MONTH_IN_MILLISECONDS = 30 * 24 * 3600 * 1000;
    private final static long WEEK_IN_MILLISECONDS = 7 * 24 * 3600 * 1000;
    private final static long DAY_IN_MILLISECONDS = 24 * 3600 * 1000;
    private final static long HOUR_IN_MILLISECONDS = 3600 * 1000;

    public EditWatchItemDialog(Context context, DbAdapter dbAdapter, Long itemId, ListView list, int tabPosition) {
        super(context);
        this.context = context;
        this.dbAdapter = dbAdapter;
        this.itemId = itemId;
        this.list = list;
        this.tabPosition = tabPosition;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_edit_watch_item);
        saveButton = (Button) findViewById(R.id.saveButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        buttonCalendar = (ImageButton) findViewById(R.id.buttonCalendar);
        itemImage = (ImageView) findViewById(R.id.itemImage);
        itemName = (TextView) findViewById(R.id.itemName);
        itemCategory = (TextView) findViewById(R.id.itemCategory);
        itemBarcode = (TextView) findViewById(R.id.itemBarcode);
        itemExpireDate = (TextView) findViewById(R.id.itemExpireDate);
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        Cursor cursor = dbAdapter.queryWatchItem(itemId);
        cursor.moveToFirst();
        String name = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.NAME));
        String barcode = cursor.getString(cursor.getColumnIndex(DbHelper.WatchItemsTableKeys.BARCODE));
        String expireDate = cursor.getString(cursor.getColumnIndex(DbHelper.WatchItemsTableKeys.EXPIRE_DATE));
        String imageURL = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.IMAGE_URL));
        int categoryInInt = cursor.getInt(cursor.getColumnIndex(DbHelper.ItemsTableKeys.CATEGORY));
        String categoryAllCaps = Category.values()[categoryInInt].name();
        String category = categoryAllCaps.charAt(0) + categoryAllCaps.substring(1).toLowerCase();
        itemName.setText(name);
        itemCategory.setText(category);
        if (imageURL!=null) {
            if (imageURL.equals("very_very_cute_mouse")) {
                Uri uri = Uri.parse("android.resource://com.hkr.freshness/drawable/very_very_cute_mouse");
                itemImage.setImageURI(uri);
            } else {
                Uri uri = Uri.parse(context.getFilesDir() + "/saved_images/" + imageURL);
                itemImage.setImageURI(uri);
            }
        }
        itemBarcode.setText(barcode);
        itemExpireDate.setText(expireDate);

        buttonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                String dateString, monthString;
                                if (dayOfMonth < 10) {
                                    dateString = "0" + dayOfMonth;
                                } else {
                                    dateString = Integer.toString(dayOfMonth);
                                }
                                if (month < 9) {
                                    monthString = "0" + Integer.toString(month+1);
                                } else {
                                    monthString = Integer.toString(month+1);
                                }
                                newExpireDate = String.format("%d-%s-%s", year, monthString, dateString);
                                itemExpireDate.setText(newExpireDate);
                                itemExpireDate.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.colorSecondary, null));
                                scheduleNotifications(year, month, dayOfMonth, itemName.getText().toString());
                            }}, currentYear, currentMonth, currentDay);
                datePickerDialog.getDatePicker().setMinDate(new Date().getTime() + DAY_IN_MILLISECONDS);
                datePickerDialog.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveButton:
                dbAdapter.updateWatchItem(itemId, itemBarcode.getText().toString(), itemExpireDate.getText().toString());
                Toast.makeText(context, "New expire date saved", Toast.LENGTH_LONG).show();
                showList(tabPosition);
                break;
            case R.id.cancelButton:
                dismiss();
                break;
            default:
                dismiss();
        }
        dismiss();
    }

    private void scheduleNotification(Notification notification, long delayInMilliSeconds) {

        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        int notificationId = UUID.randomUUID().hashCode();
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        notificationIntent.putExtra(NotificationPublisher.ITEM_ID, Long.toString(itemId));
        notificationIntent.putExtra(NotificationPublisher.ITEM_EXPIRE_DATE, newExpireDate);

        // ONE_SHOT creates a new intent instead of replacing the old one
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        long futureInMillis = SystemClock.elapsedRealtime() + delayInMilliSeconds;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    private Notification buildNotification(String title, String content) {
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setSmallIcon(R.mipmap.ic_launcher_freshness);
        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        // clear notification from notification bar after it is clicked
        builder.setAutoCancel(true);
        return builder.build();
    }

    private void scheduleNotifications(int year, int month, int dayOfMonth, String itemName) {
        Date futureDate = new Date(year-1900, month, dayOfMonth);
        Date today = new Date();

        Notification beforeExpiry = buildNotification("An item will expire soon!", itemName + " expires on " + newExpireDate);
        Notification afterExpiry = buildNotification("An item has expired!", itemName + " expired on " + newExpireDate);

        long timeDifference = futureDate.getTime() - today.getTime();

        if (timeDifference > MONTH_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - MONTH_IN_MILLISECONDS);
        }
        if (timeDifference > WEEK_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - WEEK_IN_MILLISECONDS);
        }
        if (timeDifference > DAY_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - DAY_IN_MILLISECONDS);
        } else {
            scheduleNotification(beforeExpiry, 5000);
        }
        scheduleNotification(afterExpiry, timeDifference + HOUR_IN_MILLISECONDS);
    }

    private void showList(int tabPosition) {
        Cursor cursor = null;
        String[] from = { DbHelper.WatchItemsTableKeys.BARCODE, DbHelper.WatchItemsTableKeys.EXPIRE_DATE,
                DbHelper.ItemsTableKeys.NAME, DbHelper.ItemsTableKeys.IMAGE_URL, DbHelper.ItemsTableKeys.CATEGORY};
        int[] to = { R.id.itemBarcode, R.id.itemExpireDate, R.id.itemName, R.id.itemImage, R.id.itemCategory };

        switch (tabPosition) {
            case 0:
                cursor = dbAdapter.queryAllWatchItems();
                break;
            case 1:
                cursor = dbAdapter.queryAllFoodWatchItems();
                break;
            case 2:
                cursor = dbAdapter.queryAllCosmeticsWatchItems();
                break;
            case 3:
                cursor = dbAdapter.queryAllOtherWatchItems();
                break;
        }
        ImageCursorAdapter cursorAdapter = new ImageCursorAdapter(context, R.layout.row_main, cursor, from, to);
        list.setAdapter(cursorAdapter);
    }

}
