package com.hkr.freshness;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbHelper;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A custom CursorAdapter class binding row with row view with expire date
 * @author Xujia Zhou
 */
public class ImageCursorAdapter extends SimpleCursorAdapter {

    private Cursor c;
    private Context context;

    public ImageCursorAdapter(Context context, int layout, Cursor cursor, String[] from, int[] to) {
        super(context, layout, cursor, from, to, 0);
        this.context = context;
    }

    @Override
    public void bindView(View inView, Context context, Cursor cursor) {
        View view = inView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_main, null);
        }
        String barcode = cursor.getString(cursor.getColumnIndex(DbHelper.WatchItemsTableKeys.BARCODE));
        String expireDate = cursor.getString(cursor.getColumnIndex(DbHelper.WatchItemsTableKeys.EXPIRE_DATE));
        String name = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.NAME));
        String imageURL = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.IMAGE_URL));
        int category = cursor.getInt(cursor.getColumnIndex(DbHelper.ItemsTableKeys.CATEGORY));
        String categoryValue = Category.values()[category].name();
        TextView expireOn = (TextView) view.findViewById(R.id.expireOn);
        ImageView imageView = (ImageView) view.findViewById(R.id.itemImage);
        if (imageURL != null) {
            if (imageURL.equals("very_very_cute_mouse")) {
                Uri uri = Uri.parse("android.resource://com.hkr.freshness/drawable/very_very_cute_mouse");
                 imageView.setImageURI(uri);
            } else {
                Uri uri = Uri.parse(context.getFilesDir() + "/saved_images/" + imageURL);
                imageView.setImageURI(uri);
            }
        }
        TextView itemBarcode = (TextView) view.findViewById(R.id.itemBarcode);
        itemBarcode.setText(barcode);

        TextView itemName = (TextView) view.findViewById(R.id.itemName);
        itemName.setText(name);

        TextView itemExpireDate = (TextView) view.findViewById(R.id.itemExpireDate);
        itemExpireDate.setText(expireDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date expiryDate = dateFormat.parse(expireDate);
            long expiryDateSeconds = expiryDate.getTime();
            Date today = new Date();
            long todaySeconds = today.getTime();
            if (today.after(expiryDate)) {
                itemExpireDate.setTextColor(Color.RED);
                expireOn.setText("Expired on: ");
            } else if ((expiryDateSeconds - todaySeconds) / 1000 < 720*3600) {
                itemExpireDate.setTextColor(Color.parseColor("#ffb74d"));
                expireOn.setText("Expire on: ");
            } else {
                itemExpireDate.setTextColor(Color.GRAY);
                expireOn.setText("Expire on: ");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TextView itemCategory = (TextView) view.findViewById(R.id.itemCategory);
        itemCategory.setText(categoryValue);
        if (categoryValue.equals("FOOD")) {
            itemCategory.setTextColor(ContextCompat.getColor(context, R.color.foodColor));
        } else if (categoryValue.equals("COSMETICS")) {
            itemCategory.setTextColor(ContextCompat.getColor(context, R.color.cosmeticsColor));
        } else {
            itemCategory.setTextColor(ContextCompat.getColor(context, R.color.lightGray400));
        }
    }

}