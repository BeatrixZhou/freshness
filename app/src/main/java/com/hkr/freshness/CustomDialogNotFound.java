package com.hkr.freshness;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;

import java.util.Date;
import java.util.UUID;

/**
 * Created by XUZH0001 on 5/17/2017.
 */

public class CustomDialogNotFound extends Dialog implements android.view.View.OnClickListener {

    private Button yesButton, noButton;
    private String scannedBarcode;
    private Context context;
    private TextView itemBarcode;
    public static final String EXTRA_MESSAGE = "com.hkr.freshness.MESSAGE";

    public CustomDialogNotFound(Context context, String scannedBarcode) {
        super(context);
        this.context = context;
        this.scannedBarcode = scannedBarcode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_not_found);
        itemBarcode = (TextView) findViewById(R.id.itemBarcode);
        yesButton = (Button) findViewById(R.id.yesButton);
        noButton = (Button) findViewById(R.id.noButton);
        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);
        itemBarcode.setText(scannedBarcode);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.yesButton:
                Intent intent = new Intent(context,
                        CreateItemActivity.class);
                intent.putExtra(EXTRA_MESSAGE, scannedBarcode);
                context.startActivity(intent);
                break;
            case R.id.noButton:
                dismiss();
                break;
            default:
                dismiss();
        }
        dismiss();
    }

}