package com.hkr.freshness;

import android.Manifest;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.common.StringUtils;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.os.Environment.DIRECTORY_PICTURES;

/**
 * Activity where user adds a new unique itemDelete to itemDelete list
 * @author Xujia Zhou
 */
public class CreateItemActivity extends AppCompatActivity {

    private static final int GALLERY_PERMISSIONS_REQUEST = 0;
    private static final int GALLERY_IMAGE_REQUEST = 1;
    public static final int CAMERA_PERMISSIONS_REQUEST = 2;
    public static final int CAMERA_IMAGE_REQUEST = 3;
    private static final String TAG = CreateItemActivity.class.getSimpleName();

    private Button clearButton;
    private Button okButton;
    private Spinner editCategory;
    EditText editItemName, editItemExpireDate, editItemBarcode;
    String nameValue, expireDateValue, imageURLValue, barcodeValue;
    int categoryValue;
    private DbAdapter dbAdapter;
    private ImageButton buttonCamera, buttonScanner, buttonCalendar;
    private ImageButton buttonGallery;
    private ImageView imageView;
    private Bitmap bitmap;
    long newItemId;

    private int currentYear, currentMonth, currentDay;
    private String yearString, monthString, dayString;
    private final static long MONTH_IN_MILLISECONDS = 30 * 24 * 3600 * 1000;
    private final static long WEEK_IN_MILLISECONDS = 7 * 24 * 3600 * 1000;
    private final static long DAY_IN_MILLISECONDS = 24 * 3600 * 1000;
    private final static long HOUR_IN_MILLISECONDS = 3600 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_item);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbAdapter = new DbAdapter(this);

        clearButton = (Button) findViewById(R.id.clearButton);
        okButton = (Button) findViewById(R.id.okButton);
        buttonCamera = (ImageButton) findViewById(R.id.buttonCamera);
        buttonGallery = (ImageButton) findViewById(R.id.buttonGallery);
        buttonScanner = (ImageButton) findViewById(R.id.buttonScanner);
        buttonCalendar = (ImageButton) findViewById(R.id.buttonCalendar);
        imageView = (ImageView) findViewById(R.id.imageView);
        editItemName = (EditText) findViewById(R.id.editItemName);
        editItemExpireDate = (EditText) findViewById(R.id.editItemExpireDate);
        editItemBarcode = (EditText) findViewById(R.id.editItemBarcode);
        editCategory = (Spinner) findViewById(R.id.editItemCategory);

        editCategory.setAdapter(new ArrayAdapter<Category>(this, R.layout.spinner_row, Category.values()));

        Intent intent = getIntent();
        String scannedBarcode = intent.getStringExtra(CustomDialogNotFound.EXTRA_MESSAGE);
        if (scannedBarcode!=null) {
            editItemBarcode.setText(scannedBarcode);
        }

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nameValue = editItemName.getText().toString();
                barcodeValue = editItemBarcode.getText().toString();
                expireDateValue = editItemExpireDate.getText().toString();

                if (nameValue==null || nameValue.equals("")) {
                    Toast.makeText(getBaseContext(), "Item name can't be empty", Toast.LENGTH_LONG).show();
                    return;
                }

                String pattern ="^[0-9]+$";
                Matcher matcher = Pattern.compile(pattern).matcher(barcodeValue);
                if (barcodeValue==null || !matcher.find()) {
                    Toast.makeText(getBaseContext(), "Invalid barcode", Toast.LENGTH_LONG).show();
                    return;
                }
                Cursor c = dbAdapter.queryItem(barcodeValue);
                if (c.moveToFirst()) {
                    Toast.makeText(getBaseContext(), "Barcode already exists", Toast.LENGTH_LONG).show();
                    return;
                }

                if (expireDateValue==null || expireDateValue.equals("")) {
                    Toast.makeText(getBaseContext(), "Expire date can't be empty", Toast.LENGTH_LONG).show();
                    return;
                }

                String[] strArray = nameValue.split(" ");
                StringBuilder strBuilder = new StringBuilder();
                for (int i=0; i<strArray.length; i++) {
                    String cappedStr = strArray[i].substring(0, 1).toUpperCase() + strArray[i].substring(1);
                    if (i==strArray.length-1) {
                        strBuilder.append(cappedStr);
                    } else {
                        strBuilder.append(cappedStr + " ");
                    }
                }
                String nameValueUpperCased = strBuilder.toString();

                imageURLValue = saveImage(bitmap);
                // imageURLValue = editItemImage.getText().toString();

                categoryValue = editCategory.getSelectedItemPosition();

                dbAdapter.createItem(barcodeValue, nameValueUpperCased, imageURLValue, categoryValue);
                newItemId = dbAdapter.createWatchItem(barcodeValue, expireDateValue);
                Toast.makeText(getBaseContext(), "New item created", Toast.LENGTH_LONG).show();
                scheduleNotifications(Integer.parseInt(yearString), Integer.parseInt(monthString)-1,
                        Integer.parseInt(dayString), nameValueUpperCased);

                finish();

                Intent intent = new Intent(CreateItemActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editItemName.setText("");
                editItemBarcode.setText("");
                editItemExpireDate.setText("");
            }
        });

        buttonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCamera();
            }
        });

        buttonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGalleryChooser();
            }
        });

        buttonScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(CreateItemActivity.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scanning");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
            }
        });

        buttonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateItemActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                yearString = Integer.toString(year);
                                if (dayOfMonth < 10) {
                                    dayString = "0" + dayOfMonth;
                                } else {
                                    dayString = Integer.toString(dayOfMonth);
                                }
                                if (month < 9) {
                                    monthString = "0" + Integer.toString(month+1);
                                } else {
                                    monthString = Integer.toString(month+1);
                                }
                                expireDateValue = String.format("%d-%s-%s", year, monthString, dayString);
                                editItemExpireDate.setText(expireDateValue);
                            }}, currentYear, currentMonth, currentDay);
                datePickerDialog.getDatePicker().setMinDate(new Date().getTime() + DAY_IN_MILLISECONDS);
                datePickerDialog.show();
            }
        });

    }

    public void startCamera() {
        if (PermissionUtils.requestPermission(
                this,
                CAMERA_PERMISSIONS_REQUEST,
                Manifest.permission.CAMERA)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // intent.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
            /*Uri photoUri = FileProvider.getUriForFile(this, "com.hkr.freshness.fileProvider", getCameraFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);*/
            startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
        } else {
            Toast.makeText(getBaseContext(), "Camera is not available", Toast.LENGTH_LONG).show();
        }
    }

    public void startGalleryChooser() {
        if (PermissionUtils.requestPermission(
                this,
                GALLERY_PERMISSIONS_REQUEST,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select a photo"),
                    GALLERY_IMAGE_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            /*File out = new File(getFilesDir(), "newImage.jpg");
            if(!out.exists()) {
                Toast.makeText(getBaseContext(),
                        "Error while capturing image", Toast.LENGTH_LONG)
                        .show();
                return;
            }
            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());*/
            bitmap = (Bitmap) data.getExtras().get("data");
            /*Uri imageUri = data.getData();
            imageUri.toString();
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");*/
            imageView.setImageBitmap(bitmap);
        }
        if (requestCode == GALLERY_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents()==null) {
                    Toast.makeText(this, "Scanning cancelled", Toast.LENGTH_LONG).show();
                } else {
                    String scannedBarcode = result.getContents();
                    editItemBarcode.setText(scannedBarcode);
                }
            }
        }
    }

    private String saveImage(Bitmap bitmap) {
        File dir = getFilesDir();
        String fileName = Long.toString(System.currentTimeMillis());
        File myDir = new File(dir + "/saved_images");
        if (!myDir.exists()) {
            myDir.mkdir();
        }
        File file = new File (myDir, fileName);
        try (FileOutputStream out = new FileOutputStream(file)) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    private void scheduleNotification(Notification notification, long delayInMilliSeconds) {

        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        int notificationId = UUID.randomUUID().hashCode();
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        notificationIntent.putExtra(NotificationPublisher.ITEM_ID, Long.toString(newItemId));
        notificationIntent.putExtra(NotificationPublisher.ITEM_EXPIRE_DATE, expireDateValue);

        // ONE_SHOT creates a new intent instead of replacing the old one
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, notificationId, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        long futureInMillis = SystemClock.elapsedRealtime() + delayInMilliSeconds;
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    private Notification buildNotification(String title, String content) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setSmallIcon(R.mipmap.ic_launcher_freshness);
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        // clear notification from notification bar after it is clicked
        builder.setAutoCancel(true);
        return builder.build();
    }

    private void scheduleNotifications(int year, int month, int dayOfMonth, String itemName) {
        Date futureDate = new Date(year-1900, month, dayOfMonth);
        Date today = new Date();

        Notification beforeExpiry = buildNotification("An item will expire soon!", itemName + " expires on " + expireDateValue);
        Notification afterExpiry = buildNotification("An item has expired!", itemName + " expired on " + expireDateValue);

        long timeDifference = futureDate.getTime() - today.getTime();

        if (timeDifference > MONTH_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - MONTH_IN_MILLISECONDS);
        }
        if (timeDifference > WEEK_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - WEEK_IN_MILLISECONDS);
        }
        if (timeDifference > DAY_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - DAY_IN_MILLISECONDS);
        } else {
            scheduleNotification(beforeExpiry, 5000);
        }
        scheduleNotification(afterExpiry, timeDifference + HOUR_IN_MILLISECONDS);
    }

}
