package com.hkr.freshness;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hkr.freshness.db.Category;
import com.hkr.freshness.db.DbAdapter;
import com.hkr.freshness.db.DbHelper;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.UUID;

/**
 * Created by XUZH0001 on 5/17/2017.
 */

public class CustomDialog extends Dialog implements android.view.View.OnClickListener {

    private Activity activity;
    private Dialog dialog;
    private Button yesButton, noButton;
    private TextView itemName, itemCategory, itemBarcode;
    private ImageView itemImage;
    private DbAdapter dbAdapter;
    private String scannedBarcode;
    private Context context;
    private String expireDate;
    private String newItemName;
    private long newItemId;
    private ListView list;
    private int tabPosition;

    int currentYear, currentMonth, currentDay;

    private final static long MONTH_IN_MILLISECONDS = 30 * 24 * 3600 * 1000;
    private final static long WEEK_IN_MILLISECONDS = 7 * 24 * 3600 * 1000;
    private final static long DAY_IN_MILLISECONDS = 24 * 3600 * 1000;
    private final static long HOUR_IN_MILLISECONDS = 3600 * 1000;

    public CustomDialog(Context context, DbAdapter dbAdapter, String scannedBarcode, ListView list, int tabPosition) {
        super(context);
        this.context = context;
        this.dbAdapter = dbAdapter;
        this.scannedBarcode = scannedBarcode;
        this.list = list;
        this.tabPosition = tabPosition;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        yesButton = (Button) findViewById(R.id.yesButton);
        noButton = (Button) findViewById(R.id.noButton);
        itemImage = (ImageView) findViewById(R.id.itemImage);
        itemName = (TextView) findViewById(R.id.itemName);
        itemCategory = (TextView) findViewById(R.id.itemCategory);
        itemBarcode = (TextView) findViewById(R.id.itemBarcode);
        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);

        Cursor cursor = dbAdapter.queryItem(scannedBarcode);
        cursor.moveToFirst();
        newItemName = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.NAME));
        String imageURL = cursor.getString(cursor.getColumnIndex(DbHelper.ItemsTableKeys.IMAGE_URL));
        int categoryInInt = cursor.getInt(cursor.getColumnIndex(DbHelper.ItemsTableKeys.CATEGORY));
        String categoryAllCaps = Category.values()[categoryInInt].name();
        String category = categoryAllCaps.charAt(0) + categoryAllCaps.substring(1).toLowerCase();
        itemName.setText(newItemName);
        itemCategory.setText(category);
        Uri uri = Uri.parse(context.getFilesDir() + "/saved_images/" + imageURL);
        itemImage.setImageURI(uri);
        itemBarcode.setText(scannedBarcode);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.yesButton:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                String dateString, monthString;
                                if (dayOfMonth < 10) {
                                    dateString = "0" + dayOfMonth;
                                } else {
                                    dateString = Integer.toString(dayOfMonth);
                                }
                                if (month < 9) {
                                    monthString = "0" + Integer.toString(month+1);
                                } else {
                                    monthString = Integer.toString(month+1);
                                }
                                expireDate = String.format("%d-%s-%s", year, monthString, dateString);
                                newItemId = dbAdapter.createWatchItem(scannedBarcode, expireDate);
                                Toast.makeText(context, "New item created", Toast.LENGTH_LONG).show();
                                scheduleNotifications(year, month, dayOfMonth, newItemName);
                                showList(tabPosition);
                            }}, currentYear, currentMonth, currentDay);
                datePickerDialog.getDatePicker().setMinDate(new Date().getTime() + DAY_IN_MILLISECONDS);
                datePickerDialog.show();
                break;
            case R.id.noButton:
                dismiss();
                break;
            default:
                dismiss();
        }
        dismiss();
    }

    private void scheduleNotification(Notification notification, long delayInMilliSeconds) {

        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        int notificationId = UUID.randomUUID().hashCode();
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        notificationIntent.putExtra(NotificationPublisher.ITEM_ID, Long.toString(newItemId));
        notificationIntent.putExtra(NotificationPublisher.ITEM_EXPIRE_DATE, expireDate);

        // ONE_SHOT creates a new intent instead of replacing the old one
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        long futureInMillis = SystemClock.elapsedRealtime() + delayInMilliSeconds;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    private Notification buildNotification(String title, String content) {
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setSmallIcon(R.mipmap.ic_launcher_freshness);
        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        // clear notification from notification bar after it is clicked
        builder.setAutoCancel(true);
        return builder.build();
    }

    private void scheduleNotifications(int year, int month, int dayOfMonth, String itemName) {
        Date futureDate = new Date(year-1900, month, dayOfMonth);
        Date today = new Date();

        Notification beforeExpiry = buildNotification("An item will expire soon!", itemName + " expires on " + expireDate);
        Notification afterExpiry = buildNotification("An item has expired!", itemName + " expired on " + expireDate);

        long timeDifference = futureDate.getTime() - today.getTime();

        if (timeDifference > MONTH_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - MONTH_IN_MILLISECONDS);
        }
        if (timeDifference > WEEK_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - WEEK_IN_MILLISECONDS);
        }
        if (timeDifference > DAY_IN_MILLISECONDS) {
            scheduleNotification(beforeExpiry, timeDifference - DAY_IN_MILLISECONDS);
        } else {
            scheduleNotification(beforeExpiry, 5000);
        }
        scheduleNotification(afterExpiry, timeDifference + HOUR_IN_MILLISECONDS);
    }

    private void showList(int tabPosition) {
        Cursor cursor = null;
        String[] from = { DbHelper.WatchItemsTableKeys.BARCODE, DbHelper.WatchItemsTableKeys.EXPIRE_DATE,
                DbHelper.ItemsTableKeys.NAME, DbHelper.ItemsTableKeys.IMAGE_URL, DbHelper.ItemsTableKeys.CATEGORY};
        int[] to = { R.id.itemBarcode, R.id.itemExpireDate, R.id.itemName, R.id.itemImage, R.id.itemCategory };

        switch (tabPosition) {
            case 0:
                cursor = dbAdapter.queryAllWatchItems();
                break;
            case 1:
                cursor = dbAdapter.queryAllFoodWatchItems();
                break;
            case 2:
                cursor = dbAdapter.queryAllCosmeticsWatchItems();
                break;
            case 3:
                cursor = dbAdapter.queryAllOtherWatchItems();
                break;
        }
        ImageCursorAdapter cursorAdapter = new ImageCursorAdapter(context, R.layout.row_main, cursor, from, to);
        list.setAdapter(cursorAdapter);
    }

}
