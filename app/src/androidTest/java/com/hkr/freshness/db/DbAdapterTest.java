package com.hkr.freshness.db;

import android.content.Context;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.deps.guava.collect.Sets;
import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;

import static org.junit.Assert.*;

/**
 * Test for {@link DbAdapter}
 * @author Xujia Zhou
 */
@RunWith(AndroidJUnit4.class)
public class DbAdapterTest {

    private Context context;
    private DbAdapter dbAdapter;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getTargetContext();
        dbAdapter = new DbAdapter(context);
    }

    @After
    public void teardown() {
        dbAdapter.deleteAllWatchItems();
        dbAdapter.deleteAllItems();
    }

    @Test
    public void createItem() throws Exception {
        String expectedBarcode = "774540626604";
        String expectedName = "Pingvin Ferrari Salt";
        String expectedImageName = "pingvin_ferrari_salt";
        int expectedCategory = 0;
        dbAdapter.createItem(expectedBarcode, expectedName, expectedImageName, expectedCategory);
        Cursor c = dbAdapter.queryItem("774540626604");
        c.moveToFirst();
        String actualBarcode = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.BARCODE_ALIAS));
        String actualName = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.NAME));
        String actualImageName = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.IMAGE_URL));
        int actualCategory = c.getInt(c.getColumnIndex(DbHelper.ItemsTableKeys.CATEGORY));

        assertEquals(expectedBarcode, actualBarcode);
        assertEquals(expectedName, actualName);
        assertEquals(expectedImageName, actualImageName);
    }

    @Test
    public void queryAllItems() throws Exception {
        Item item1 = new Item("1234", "1234", "1234", Category.FOOD);
        Item item2 = new Item("5678", "5678", "5678", Category.COSMETICS);
        Item item3 = new Item("6666", "6666", "6666", Category.OTHER);

        dbAdapter.createItem(item1.getBarcode(), item1.getName(), item1.getImageURL(), item1.getCategory().ordinal());
        dbAdapter.createItem(item2.getBarcode(), item2.getName(), item2.getImageURL(), item2.getCategory().ordinal());
        dbAdapter.createItem(item3.getBarcode(), item3.getName(), item3.getImageURL(), item3.getCategory().ordinal());

        Cursor c = dbAdapter.queryAllItems();

        Set<Item> expectedItems = Sets.newHashSet(item1, item2, item3);

        Set<Item> actualItems = Sets.newHashSet();

        while (c.moveToNext()) {
            Item item = getItemFromCursorRow(c);
            actualItems.add(item);
        }

        assertEquals(expectedItems, actualItems);
    }

    @Test
    public void queryAllWatchItems() throws Exception {

    }

    private Item getItemFromCursorRow(Cursor c) {
        String barcode = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.BARCODE_ALIAS));
        String name = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.NAME));
        String imageName = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.IMAGE_URL));
        String category = c.getString(c.getColumnIndex(DbHelper.ItemsTableKeys.CATEGORY));
        return new Item(barcode, name, imageName, Category.valueOf(category));
    }

}